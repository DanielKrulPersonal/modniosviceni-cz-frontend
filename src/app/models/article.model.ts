export interface Article {
    title: string;
    text: string;
    perex: string;
    url: string;
    categoryUrl: string;
    categoryName: string;
    image: string;
}
