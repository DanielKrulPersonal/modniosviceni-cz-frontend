import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { MarkdownService } from 'ngx-markdown';

@Component({
    selector: 'app-about-me',
    templateUrl: './aboutMe.component.html',
    styleUrls: ['./aboutMe.component.less']
})
export class AboutMeComponent implements OnInit {
    aboutMeText: string;

    constructor(
        private readonly firebaseService: FirebaseService,
        private markdownService: MarkdownService
    ) {
        firebaseService.getConfig('aboutMe-text').then(config => {
            this.aboutMeText = this.markdownService.compile(JSON.parse(config.value));
        });
    }

    ngOnInit() {
    }

}
