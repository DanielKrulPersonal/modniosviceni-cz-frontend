import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Article } from '../models/article.model';
import { Category } from '../models/category.model';

@Injectable({
    providedIn: 'root'
})
export class FirebaseService {

    constructor(
        private readonly db: AngularFirestore,
    ) { }

    getConfig(name: string) {
        return new Promise<any>((resolve, reject) => {
            const col = this.db.collection('/config', ref => ref.where('name', '==', name).limit(1));
            col.valueChanges().subscribe(snapshots => {
                resolve(snapshots[0]);
            });
        });
    }

    getMarkedArticles() {
        return new Promise<any>((resolve, reject) => {
            const col = this.db.collection('/articles', ref => ref.orderBy('date', 'desc').where('marked', '==', true));
            col.valueChanges().subscribe(snapshots => {
                resolve(snapshots);
            });
        });
    }

    getArticleByUrl(url: string): Promise<Article> {
        return new Promise<any>((resolve, reject) => {
            const col = this.db.collection('/articles', ref => ref.where('url', '==', url).limit(1));
            col.valueChanges().subscribe(snapshots => {
                resolve(snapshots[0]);
            });
        });
    }

    getCategoryNameByUrl(url: string) {
        return new Promise<any>((resolve, reject) => {
            const col = this.db.collection('/categories', ref => ref.where('url', '==', url).limit(1));
            col.valueChanges().subscribe(snapshots => {
                const category = snapshots[0] as Category;
                resolve(category.name);
            });
        });
    }

    getArticles(): Promise<Article[]> {
        return new Promise<any>((resolve, reject) => {
            const col = this.db.collection('/articles', ref => ref.orderBy('date', 'desc'));
            col.valueChanges().subscribe(snapshots => {
                const articles = snapshots as Article[];
                articles.shift();
                resolve(articles);
            });
        });
    }

    getArticlesByCategory(category: string): Promise<Article[]> {
        return new Promise<any>((resolve, reject) => {
            const col = this.db.collection('/articles', ref => ref.where('categoryUrl', '==', category).orderBy('date', 'desc'));
            col.valueChanges().subscribe(snapshots => {
                resolve(snapshots);
            });
        });
    }

    getCategories(): Promise<Category[]> {
        return new Promise<any>((resolve, reject) => {
            const col = this.db.collection('/categories');
            col.valueChanges().subscribe(snapshots => {
                resolve(snapshots);
            });
        });
    }
}
