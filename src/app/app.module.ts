import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './shared/layout/headers/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './shared/layout/footers/footer.component';
import { RouterModule, Routes } from '@angular/router';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AboutMeComponent } from './aboutMe/aboutMe.component';
import { MarkdownModule } from 'ngx-markdown';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleComponent } from './article/article.component';
import { DisqusModule } from 'ngx-disqus';
import { SafeHtmlPipe } from './pipes/safe';

const routes: Routes = [
    { path: '', redirectTo: '/domu', pathMatch: 'full' },
    { path: 'domu', component: HomeComponent },
    { path: 'omne', component: AboutMeComponent },
    { path: 'clanky/:categoryUrl', component: ArticlesComponent },
    { path: 'clanek/:url', component: ArticleComponent },
];

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        HomeComponent,
        AboutMeComponent,
        ArticlesComponent,
        ArticleComponent,
        SafeHtmlPipe
    ],
    imports: [
        RouterModule.forRoot(routes),
        BrowserModule,
        NgbModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        MarkdownModule.forRoot(),
        DisqusModule.forRoot('modniosviceni-cz')
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
