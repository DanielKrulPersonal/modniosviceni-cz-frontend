import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';
import { Category } from '../models/category.model';
import { Article } from '../models/article.model';
import { MarkdownService } from 'ngx-markdown';

@Component({
    selector: 'app-articles',
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.less']
})
export class ArticlesComponent implements OnInit {
    currentCategory: Category = { url: '', name: '' };
    articles: Article[];

    constructor(
        private readonly route: ActivatedRoute,
        private readonly firebaseService: FirebaseService,
        private readonly markdownService: MarkdownService
    ) {
        this.currentCategory.url = this.route.snapshot.paramMap.get('categoryUrl');

        this.firebaseService.getCategoryNameByUrl(this.currentCategory.url).then(categoryName => this.currentCategory.name = categoryName);

        this.firebaseService.getArticlesByCategory(this.currentCategory.url).then(articles => {
            articles.forEach(article => {
                this.firebaseService.getCategoryNameByUrl(article.categoryUrl).then(category => {
                    article.categoryName = category;
                    article.perex = this.markdownService.compile(JSON.parse(article.perex));
                });
            });
            this.articles = articles;
        });
    }

    ngOnInit() {
    }

}
