import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';
import { Article } from '../models/article.model';
import { MarkdownService } from 'ngx-markdown';


@Component({
    selector: 'app-article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.less']
})
export class ArticleComponent implements OnInit {
    currentArticle: Article = { title: '', text: '', perex: '', url: '', categoryUrl: '', categoryName: '', image: '' };

    constructor(
        private readonly route: ActivatedRoute,
        private readonly firebaseService: FirebaseService,
        private readonly markdownService: MarkdownService,
    ) {
        this.currentArticle.url = this.route.snapshot.paramMap.get('url');

        this.firebaseService.getArticleByUrl(this.currentArticle.url).then(article => {
            article.perex = this.markdownService.compile(JSON.parse(article.perex));
            article.text = this.markdownService.compile(JSON.parse(article.text));
            this.currentArticle = article;
            this.loadPinit();
        });
    }

    ngOnInit() {

    }

    loadPinit() {
        (function (d) {
            console.log('Ahoj');
            var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
            //@ts-ignore
            p.type = 'text/javascript';
            //@ts-ignore
            p.async = true;
            //@ts-ignore
            p.src = '//assets.pinterest.com/js/pinit.js';
            f.parentNode.insertBefore(p, f);
        }(document));
    }

}
