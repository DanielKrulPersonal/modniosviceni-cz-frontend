import { Component, OnDestroy, HostListener, AfterViewInit } from '@angular/core';
import { FirebaseService } from '../../../services/firebase.service';
import { Category } from '../../../models/category.model';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnDestroy, AfterViewInit {
    showFullWidthLogo = true;
    facebookUrl: string;
    instagramUrl: string;
    youtubeUrl: string;
    categories: Category[];
    isMenuShown = false;

    constructor(
        private readonly firebaseService: FirebaseService,
    ) {
        firebaseService.getConfig('facebook-url').then(config => {
            this.facebookUrl = config.value;
        });
        firebaseService.getConfig('instagram-url').then(config => {
            this.instagramUrl = config.value;
        });
        firebaseService.getConfig('youtube-url').then(config => {
            this.youtubeUrl = config.value;
        });
        firebaseService.getCategories().then(categories => this.categories = categories);

        this.onResize(window.innerWidth);
    }

    ngOnDestroy() {
    }

    ngAfterViewInit() {

    }

    @HostListener('window:resize', ['$event.target.innerWidth'])
    onResize(width: number) {
        this.showFullWidthLogo = width > 992;
    }

    toggleMenu() {
        this.isMenuShown = !this.isMenuShown;
    }
}
