import { Component, OnInit, HostListener } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { MarkdownService } from 'ngx-markdown';
import { Article } from '../models/article.model';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
    markedArticles: Article[];
    articles: Article[];
    showSlideshow = true;

    constructor(
        private readonly firebaseService: FirebaseService,
        private readonly markdownService: MarkdownService
    ) {
        this.firebaseService.getMarkedArticles().then(articles => {
            articles.forEach(article => {
                this.firebaseService.getCategoryNameByUrl(article.categoryUrl).then(category => {
                    article.categoryName = category;
                    article.perex = this.markdownService.compile(JSON.parse(article.perex));
                });
            });

            this.markedArticles = articles;
        });

        this.firebaseService.getArticles().then(articles => {
            articles.forEach(article => {
                this.firebaseService.getCategoryNameByUrl(article.categoryUrl).then(category => {
                    article.categoryName = category;
                    article.perex = this.markdownService.compile(JSON.parse(article.perex));
                });
            });

            this.articles = articles;
        });

        this.onResize(window.innerWidth);
    }

    ngOnInit() {
    }

    @HostListener('window:resize', ['$event.target.innerWidth'])
    onResize(width: number) {
        this.showSlideshow = width > 992;
    }
}
