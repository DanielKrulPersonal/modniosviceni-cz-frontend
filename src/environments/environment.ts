// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyAiEVW0EenoSRibuFDmtU-TDnt8SAdfyrU',
        authDomain: 'modniosviceni-cz.firebaseapp.com',
        databaseURL: 'https://modniosviceni-cz.firebaseio.com',
        projectId: 'modniosviceni-cz',
        storageBucket: 'modniosviceni-cz.appspot.com',
        messagingSenderId: '429152515666',
        appId: '1:429152515666:web:9546fbff055287c7'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
